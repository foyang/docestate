package com.hz.docEstate.repository;

import com.hz.docEstate.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDAO extends JpaRepository<Product, Integer> {
    Product findByName(String name);
    Product findById(int id);
}
