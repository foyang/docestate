package com.hz.docEstate.controller;

import com.hz.docEstate.model.Product;
import com.hz.docEstate.model.User;
import com.hz.docEstate.repository.ProductDAO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("products")
@CrossOrigin
@Api(description = "This service allows you to make the CRUD for the products.")
public class ProductController {

    @Autowired
    private ProductDAO productDAO;

    @PostMapping("add")
    public ResponseEntity<?> addProduct(@Valid @RequestBody Product product){
        Product productByName = productDAO.findByName(product.getName());
        if (productByName != null){
            return new ResponseEntity<>("A product with name "+product.getName()+" already exists.",HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity<>(productDAO.save(product), HttpStatus.CREATED);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getProductById(@PathVariable("id") int id){
        Product productById = productDAO.findById(id);
        if (productById == null){
            return new ResponseEntity<>("No product with this id "+id+" has been found",HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(productById,HttpStatus.OK);
        }
    }

    @GetMapping("")
    public ResponseEntity<?> getProducts(){
        List<Product> products = productDAO.findAll();
        if (products.size() == 0){
            return new ResponseEntity<>("No product has been found",HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(products,HttpStatus.OK);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id){
        Product product = productDAO.findById(id);
        if (product == null){
            return new ResponseEntity<>("No product with this id "+id+" has been found",HttpStatus.BAD_REQUEST);
        }else {
            productDAO.delete(product);
        }
        return null;
    }

    @DeleteMapping("")
    public ResponseEntity<?> deleteProducts() {
        List <Product> products = productDAO.findAll();
        if (products.size() == 0){
            return new ResponseEntity<>("No product has been found",HttpStatus.NOT_FOUND);
        } else {
            productDAO.deleteAll(products);
            return new ResponseEntity<>("The Products have been successfully deleted",HttpStatus.OK);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @Valid @RequestBody Product product){
        Product productById = productDAO.findById(id);
        if (productById == null){
            return new ResponseEntity<>("No product with this id "+id+" has been found",HttpStatus.BAD_REQUEST);
        } else {
            productById.setName(product.getName());
            productById.setType(product.getType());
            productById.setPrice(product.getPrice());
            productDAO.save(productById);
            return new ResponseEntity<>(productById,HttpStatus.OK);
        }
    }
    @RequestMapping({ "/validateLogin" })
    public User validateLogin() {
        return new User("User successfully authenticated");
    }
}
