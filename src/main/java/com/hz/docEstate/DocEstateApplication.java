package com.hz.docEstate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocEstateApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocEstateApplication.class, args);
	}

}
